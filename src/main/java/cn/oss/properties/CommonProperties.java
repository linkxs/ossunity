package cn.oss.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Objects;

/**
 * 通用配置信息
 *
 * @author 轻描淡写 linkxs@qq.com
 */
@ConfigurationProperties(prefix = "oss")
public class CommonProperties {

    private String type;

    private Boolean auto;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Boolean getAuto() {
        if(Objects.isNull(auto)){
            this.auto = false;
        }
        return auto;
    }

    public void setAuto(Boolean auto) {
        this.auto = auto;
    }

}
