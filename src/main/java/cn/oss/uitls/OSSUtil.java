package cn.oss.uitls;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class OSSUtil {

    public static String getPath(String path) {
        // 使用字符串操作获取文件名
        int lastIndex = path.lastIndexOf("/");
        String filePath = path.substring(0, lastIndex + 1);
        // 根据当前时间创建目录
        LocalDate currentDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMdd");
        String customFormattedDate = currentDate.format(formatter);
        String fileName = path.substring(lastIndex + 1);
        String newFileName = fileName.replaceAll("^\\d+", String.valueOf(System.currentTimeMillis()));
        return filePath + customFormattedDate + "/" + newFileName;
    }
}
