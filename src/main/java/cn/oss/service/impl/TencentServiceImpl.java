package cn.oss.service.impl;

import cn.oss.properties.CommonProperties;
import cn.oss.properties.TencentProperties;
import cn.oss.service.OSSUnity;
import cn.oss.uitls.OSSUtil;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.region.Region;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.File;
import java.io.InputStream;

/**
 * 腾讯云OSS
 *
 * @author 轻描淡写 linkxs@qq.com
 */
public class TencentServiceImpl implements OSSUnity {
    @Resource
    private TencentProperties properties;
    @Resource
    private CommonProperties commonProperties;

    private static COSClient client;

    @PostConstruct
    private void init() {
        BasicCOSCredentials cred = new BasicCOSCredentials(properties.getAppid(), properties.getSecretId(), properties.getSecretKey());
        Region region = new Region(properties.getRegion());
        ClientConfig clientConfig = new ClientConfig(region);
        client = new COSClient(cred, clientConfig);
    }

    @Override
    public String upload(String path, File file) {
        String ossPath = commonProperties.getAuto() ? OSSUtil.getPath(path) : path;
        client.putObject(properties.getBucketName(), ossPath, file);
        return properties.getDomain() + "/" + ossPath;
    }

    @Override
    public String upload(String path, InputStream inputStream) {
        String ossPath = commonProperties.getAuto() ? OSSUtil.getPath(path) : path;
        client.putObject(properties.getBucketName(), ossPath, inputStream, null);
        return properties.getDomain() + "/" + ossPath;
    }
}
