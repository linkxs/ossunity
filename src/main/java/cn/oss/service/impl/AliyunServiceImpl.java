package cn.oss.service.impl;

import cn.oss.properties.AliyunProperties;
import cn.oss.properties.CommonProperties;
import cn.oss.service.OSSUnity;
import cn.oss.uitls.OSSUtil;
import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.File;
import java.io.InputStream;


/**
 * 阿里云服务实现
 *
 * @author 轻描淡写 linkxs@qq.com
 */
public class AliyunServiceImpl implements OSSUnity {
    @Resource
    private AliyunProperties aliyunProperties;
    @Resource
    private CommonProperties commonProperties;

    private static OSS oss;

    @PostConstruct
    private void init() {
        try {
            oss = new OSSClientBuilder().build(
                    aliyunProperties.getEndpoint(),
                    aliyunProperties.getAccessKeyId(),
                    aliyunProperties.getAccessKeySecret()
            );
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    public String upload(String path, File file) {
        String ossPath = commonProperties.getAuto() ? OSSUtil.getPath(path) : path;
        oss.putObject(aliyunProperties.getBucketName(), ossPath, file);
        return aliyunProperties.getDomain() + "/" + ossPath;
    }

    @Override
    public String upload(String path, InputStream inputStream) {
        String ossPath = commonProperties.getAuto() ? OSSUtil.getPath(path) : path;
        oss.putObject(aliyunProperties.getBucketName(), ossPath, inputStream);
        return aliyunProperties.getDomain() + "/" + ossPath;
    }
}
